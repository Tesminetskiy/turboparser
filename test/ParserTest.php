<?php

use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase {

    /**
     * @param string $request
     * @param string $response
     * @dataProvider testProcessProvider
     */
    public function testProcess($request, $response) {
        $obj = new StringParser\Parser(new StringParser\JobFromJson($request));
        self::assertEquals($response, $obj->getJson());
    }

    public function testProcessProvider() {
        return array(
            array(
                json_encode(array(
                    'job' =>
                    array(
                        'text' => 'Привет, мне на <a href=\\"test@test.ru\\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!',
                        'methods' =>
                        array(
                            'stripTags',
                            'htmlspecialchars',
                            'replaceSpacesToEol',
                            'removeSymbols',
                            'removeSpaces',
                            'toNumber',
                        ),
                    ),
                        ), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                json_encode(array(
                    'text' => 'Приветмненаtesttestruпришлоприглашениевстретитьсяпопитькофес10содержаниеммолоказа5пойдемвместе',
                        ), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
            ),
        );
    }

}
