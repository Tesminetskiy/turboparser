<?php

require __DIR__ . '/vendor/autoload.php';

$json = $_REQUEST['data'] ?? (new JobGenerator)->getJson();
echo new StringParser\Parser(new StringParser\JobFromJson($json));

