<?php

class JobGenerator {

    private $_listText = [
        'Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!',
        'Text 2',
        'Text 3.14',
        'Text 1.2, 2.3, 3.4, 4.5, 5.6, 6.7, 7.8, 9.10',
    ];
    private $_listMthods = [
        "stripTags", "removeSpaces", "replaceSpacesToEol", "htmlspecialchars", "removeSymbols", "toNumber"
    ];

    public function __construct() {
        shuffle($this->_listText);
        shuffle($this->_listMthods);
    }

    public function getArray() {
        return [
            'job' => [
                'text' => $this->_getText(),
                'methods' => $this->_getMethods()
            ]
        ];
    }

    public function getJson() {
        return json_encode($this->getArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    private function _getText() {
        return current($this->_listText);
    }

    private function _getMethods() {
        return $this->_listMthods;
    }

}
