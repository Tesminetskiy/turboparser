<?php

use Monolog\Logger as MLogger;
use Monolog\Handler\StreamHandler;

class Logger {

    private static $_logger;

    private function __construct() {
        self::$_logger = new MLogger('name');
        self::$_logger->pushHandler(new StreamHandler(sprintf('logs/%s-log.log', date('Y-m-d')), MLogger::INFO));
    }

    public static function log() {
        if (!self::$_logger) {
            new self;
        }
        return self::$_logger;
    }

    public static function __callStatic($name, $arguments) {
        call_user_func_array([self::log(), $name], $arguments);
    }

}
