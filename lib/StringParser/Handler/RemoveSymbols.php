<?php

namespace StringParser\Handler;

use StringParser\Interfaces\IMethod;

class RemoveSymbols implements IMethod {

    public function handle(string $string) {
        return preg_replace('~[\[.,/!@#$%^&*()\]]~', '', $string);
    }

}
