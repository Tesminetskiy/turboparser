<?php

namespace StringParser\Handler;

use StringParser\Interfaces\IMethod;

class RemoveSpaces implements IMethod {

    public function handle(string $string) {
        return preg_replace('/\s/', '', $string);
    }

}
