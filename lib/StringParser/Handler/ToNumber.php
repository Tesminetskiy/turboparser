<?php

namespace StringParser\Handler;

use StringParser\Interfaces\IMethod;

class ToNumber implements IMethod {

    public function handle(string $string) {
        return preg_replace_callback('/\d+\.\d+/', function($m) {
            return intval(round($m[0]));
        }, $string);
    }

}
