<?php

namespace StringParser\Handler;

use StringParser\Interfaces\IMethod;

class StripTags implements IMethod {

    public function handle(string $string) {
        return strip_tags($string);
    }

}
