<?php

namespace StringParser\Handler;

use StringParser\Interfaces\IMethod;

class ReplaceSpacesToEol implements IMethod {

    public function handle(string $string) {
        return preg_replace_callback('/\s+/', function($m) {
            return PHP_EOL;
        }, $string);
    }

}
