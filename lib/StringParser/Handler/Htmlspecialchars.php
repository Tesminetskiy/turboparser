<?php

namespace StringParser\Handler;

use StringParser\Interfaces\IMethod;

class Htmlspecialchars implements IMethod {

    public function handle(string $string) {
        return htmlspecialchars($string);
    }

}
