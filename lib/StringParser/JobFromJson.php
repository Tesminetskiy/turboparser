<?php

namespace StringParser;

use StringParser\Interfaces\IJob;

class JobFromJson implements IJob {

    private $_text;
    private $_methods = [];

    public function __construct(string $json) {
        $obj = json_decode($json);
        $this->_text = trim($obj->job->text ?? '');
        $this->_methods = !empty($obj->job->methods) ? array_map('trim', (array) $obj->job->methods) : [];
    }

    public function getText() {
        return $this->_text;
    }

    public function getMethods() {
        return $this->_methods;
    }

}
