<?php

namespace StringParser;

use StringParser\Interfaces\IJob;
use StringParser\Interfaces\IMethod;

class Parser {

    private $_job;
    private $_result;
    private $_errors = [];

    public function __construct(IJob $job) {
        $this->_job = $job;
        $this->_handle();
    }

    public function getArray() {
        return [
            'text' => $this->_result
        ];
    }

    public function getJson() {
        if (is_null($this->_result)) {
            $this->_handle();
        }
        return json_encode($this->getArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    public function __toString() {
        return $this->getJson();
    }

    private function _handle() {
        $this->_result = $this->_job->getText();
        $methods = $this->_job->getMethods();
        if (count($methods)) {
            foreach ($this->_job->getMethods() as $method) {
                $className = 'StringParser\\Handler\\' . ucfirst($method);
                if (class_exists($className)) {
                    $handler = new $className;
                    if ($handler instanceof IMethod) {
                        $this->_result = call_user_func([$handler, 'handle'], $this->_result);
                    }
                } else {
                    $this->_addError('Method: ' . $method);
                }
            }
        }
    }

    public function getErrors() {
        return $this->_errors;
    }

    public function hasErrors() {
        return (bool) $this->_errors;
    }

    private function _addError(string $err) {
        array_push($this->_errors, $err);
        \Logger::error($err);
    }

}
