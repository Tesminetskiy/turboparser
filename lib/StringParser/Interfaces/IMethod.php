<?php

namespace StringParser\Interfaces;

interface IMethod {

    public function handle(string $string);
}
