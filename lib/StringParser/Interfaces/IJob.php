<?php

namespace StringParser\Interfaces;

interface IJob {

    public function getText();

    public function getMethods();
}
